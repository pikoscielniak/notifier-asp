import { Component, OnInit } from '@angular/core';
import { EventListComponent } from './+event-list/index';
import { Routes , ROUTER_DIRECTIVES} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
@Routes([
  {path: '/', component: EventListComponent}
])
export class DashboardComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
