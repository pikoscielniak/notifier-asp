import {Injectable} from '@angular/core';

declare var Oidc:any;

interface IAuthConfig {
    authority:string;
    client_id:string;
}

@Injectable()
export class AuthManagerService {
    private config = {
        authority: "http://test:22530/",
        client_id: "js_oidc",
        redirect_uri: window.location.protocol + "//" + window.location.host + "/home/index/",
        post_logout_redirect_uri: window.location.protocol + "//" + window.location.host + "/home/index/",
        response_type: "id_token token",
        scope: "openid profile email notifier_api",
        silent_redirect_uri: window.location.protocol + "//" + window.location.host + "/auth/silent_renew.html",
        silent_renew: true,
        filter_protocol_claims: true
    };

    private oidcMgr:any;

    private log(data: string){
        console.log(data);
    }

    constructor() {
        var cfg:IAuthConfig = (<any>window).notifierSettings.authConfig;
        this.config.authority = cfg.authority;
        this.config.client_id = cfg.client_id;
        this.oidcMgr = new Oidc.UserManager(this.config);

        this.oidcMgr.signinRedirectCallback().then(function(user) {
            this.log("signed in", user);
        }).catch(function(err) {
            this.log(err);
        });
    }

    getToken() {
        this.oidcMgr.signinRedirect({data:'some data'}).then(function() {
            this.log("signinRedirect done");
        }).catch(function(err) {
            this.log(err);
        });
    }
}