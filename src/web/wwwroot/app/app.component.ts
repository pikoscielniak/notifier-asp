import {Component, OnInit} from '@angular/core';
import {Router, Routes, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';
import {MainPageComponent} from "./+main-page/main-page.component";
import {DashboardComponent} from "./+dashboard/dashboard.component";
import {AuthManagerService} from "./shared/index";

@Component({
    moduleId: module.id,
    selector: 'notifier',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css'],
    directives: [ROUTER_DIRECTIVES],
    providers: [
        ROUTER_PROVIDERS,
        AuthManagerService
    ]
})
@Routes([
    {path: '/', component: MainPageComponent},
    {path: '/dashboard', component: DashboardComponent}
])
export class AppComponent implements OnInit {

    constructor(private _router:Router,
                private _authManager:AuthManagerService) { //this unused reference is needed to have router working correctly
    }

    ngOnInit() {
        this._authManager.getToken();
        console.log("ok");
    }
}