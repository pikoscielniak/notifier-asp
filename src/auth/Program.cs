﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Notifier.Auth
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:22530")
                .Build();

            host.Run();
        }
    }
}
