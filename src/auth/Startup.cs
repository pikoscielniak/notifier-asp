﻿using System.IO;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Notifier.Auth.Configuration;

namespace Notifier.Auth
{
    public class Startup
    {
        private readonly IHostingEnvironment _environment;

        public Startup(IHostingEnvironment env)
        {
            _environment = env;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json")
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            var cert = new X509Certificate2(Path.Combine(_environment.ContentRootPath,
                "idsrv4test.pfx"), "idsrv3test");

            services.AddIdentityServer()
                  .SetSigningCredentials(cert)
                  .AddInMemoryClients(Clients.Get())
                  .AddInMemoryScopes(Scopes.Get())
                  .AddInMemoryUsers(Users.Get());

//            builder.AddCustomGrantValidator<CustomGrantValidator>();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // for the UI
            services
                .AddCors()
                .AddMvc()
                .AddRazorOptions(razor =>
                {
                    razor.ViewLocationExpanders.Add(
                        new Host.UI.CustomViewLocationExpander());
                });

            var corsPolicy = new Microsoft.AspNetCore.Cors.Infrastructure.CorsPolicy();

            corsPolicy.Headers.Add("*");
            corsPolicy.Methods.Add("*");
            corsPolicy.Origins.Add("*");
            corsPolicy.SupportsCredentials = true;
            services.AddCors(x => x.AddPolicy("corsGlobalPolicy", corsPolicy));

            services.AddTransient<Host.UI.Login.LoginService>();
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Information);
            loggerFactory.AddDebug(LogLevel.Information);

            app.UseDeveloperExceptionPage();
            app.UseCors("corsGlobalPolicy");

            app.UseIdentityServer();


            var extOptions = new CookieAuthenticationOptions
            {
                AuthenticationScheme = "External"
            };
            app.UseCookieAuthentication(extOptions);

            app.UseCookieAuthentication(extOptions);

            var googleOpt = new GoogleOptions
            {
                AuthenticationScheme = "Google",
                SignInScheme = "External",

                ClientId = Configuration["GoogleIdentityProvider:ClientId"],
                ClientSecret = Configuration["GoogleIdentityProvider:ClientSecret"],
                CallbackPath = new PathString("/googlecallback")
            };

            app.UseGoogleAuthentication(googleOpt);

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
